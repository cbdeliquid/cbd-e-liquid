What is CBD vape cartridge?
 

A CBD Vape cartridge is a pre-filled cartridge that can be attached to pens or compatible e-cigarette batteries.

 

Some are a designed for single use and some are refillable.

 

They are specifically designed to vaporise cbd e-liquids and vape oils.

 

They are referred to as vape pens because the device’s design closely resembles that of a traditional pen.

 

A vape pen consists of two pieces: a battery and cartridge.

 

 

How long does it take for CBD to take effect?
 

Inhalation by Vaping CBD is the fastest means of delivering the compound to the body and experiencing its beneficial effects. CBD enters the bloodstream directly through the lungs and the effects are noticed about 5 to 10 minutes after vaping CBD.

 

 

Whats in CBD E-Liquids?
 

Other than CBD the main two ingredients found in CBD e-liquid are propylene glycol and vegetable glycerin.

 

These are both USP grade meaning that they are produced according to the highest industry standards and tested for compliance with specifications of United States Pharmacopeia. The specified purity is more than 99.8%.

 

Propylene glycol (PG) and vegetable glycerin (VG) are common ingredients in food, drinks, medicines, smoke machine vapour, cosmetics, and many more products.

 

Here at http://www.cbdeliquid.eu/ we only use USP grade PG and VG in our CBD E-Liquids and food grade flavourings.

 

Made in the U.K in State of the art production facilities.

 

Does CBD e liquid get you high?
 

In short, no CBD will not get you high. Unlike THC, CBD is naturally non-psychoactive.

 

Can you take CBD e liquid orally?
 

Our CBD eliquids are designed for use in electronic vaporising devices only and are not suitable to consume orally.

 

Can you get addicted to CBD?
 

There is no evidence that CBD is addictive. When we take a more in-depth examination on how addiction works, addictive substances trigger the brain’s pleasure centres with large amounts of dopamine and over time, the body becomes dependant on the substance, experiencing withdrawal symptoms if an individual was to decide to stop.

 

Instead, CBD really works to balance the degree of neurochemicals within the brain - if there's an excessive amount of of 1 neurotransmitter, CBD can facilitate to lower it, and if there isn’t enough of another, CBD can facilitate bring it up to a balanced level. Plus, as a result of CBD isn't addictive, if one was to stop taking it, they wouldn’t experience any “cold turkey” withdrawal symptoms.

 